import Vue from 'vue';
import Vuex from 'vuex';
import surveys from './surveys';
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    surveys
  },
  strict: debug
});
