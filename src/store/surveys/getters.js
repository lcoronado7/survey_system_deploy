export function getQuestions(state) {
  return state.questions;
}

export function getAnswers(state) {
  return state.answers;
}

export function getAverages(state) {
  return state.averages;
}

export function getCounters(state) {
  return state.counters;
}

export function getSurvey(state) {
  return state.survey;
}
