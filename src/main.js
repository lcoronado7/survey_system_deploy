window.Vue = require('vue');
import Vue from 'vue';
import App from './App.vue'
import router from './router'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import './helpers'

import Store from './store/store';
Vue.use(Store);
Vue.config.productionTip = false

import VueCharts from 'vue-chartjs'
Vue.use(VueCharts);



import Notifications from 'vue-notification'
Vue.use(Notifications);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
