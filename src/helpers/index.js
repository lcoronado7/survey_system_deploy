// Vue
import Vue from 'vue';
import ControllerForm from './controllerForm';
import ActionNotify from './ActionNotify';


const myClass = {
  controllerForm: ControllerForm,
  actionNotify:ActionNotify
}

// Install
for (var key in myClass) Vue.prototype['$'+key] = myClass[key];
