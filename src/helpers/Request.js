
var headersGenerals = {};

export default class  {

 
  static async make(method, url, data = null, headers = null){

    if (!headers)
        headers = {
        'Content-Type': 'application/json',
        ...headersGenerals
        };


    // let myInit = { method, headers, mode: 'cors', cache: 'default' };
    let myInit = { method, headers, cache: 'default' };

    if(data) myInit.body = JSON.stringify(data)

    const request = new Request(url, myInit);

    return fetch(request).then(async res => {

        try {
          return { data: await res.clone().json(), status:res.status };
        } catch (e) {
          return { data: await res.clone().text(), status:res.status };
        }

    })
    .catch(function(error) {
      throw error.message;
    });
        
  }

}
