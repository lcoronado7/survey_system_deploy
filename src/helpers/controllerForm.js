
export default class {
    static validateForm(formID, required){

        var emptyRequiredField = false

        if(typeof(required)=='string' && required=='all'){

            var formElement = document.getElementById(formID)

            var inputElements = formElement.getElementsByTagName('input');
            var selectElements = formElement.getElementsByTagName('select');
    
            
            

            for(let element of inputElements){
                
                if(!(element.value)){
                    element.style.border="2px solid #DB4141"

                    if(!emptyRequiredField)
                        emptyRequiredField = true
                    
                }else{
                    element.style.border=""
          
                }
            }

            for(let element of selectElements){

                if(!(element.value)){
                    element.style.border="2px solid #DB4141"
                    if(!emptyRequiredField)
                        emptyRequiredField = true
                }else{
                    element.style.border=""
                    
                }
            }

        }

        return emptyRequiredField
    }

    static clearForm(formID){

        var formElement = document.getElementById(formID)
        var inputElements = formElement.getElementsByTagName('input');
        var selectElements = formElement.getElementsByTagName('select');
     
        for(let element of inputElements){
            element.value = ""
        }

        for(let element of selectElements){

            element.value = ""
        }
    }

    static desativeForm(formID){
        var formElement = document.getElementById(formID)
        var inputElements = formElement.getElementsByTagName('input');
        var selectElements = formElement.getElementsByTagName('select');
        var buttonElements = formElement.getElementsByTagName('button');
        
        for(let element of inputElements){
            element.disabled = true
        }

        for(let element of selectElements){

            element.disabled = true
        }

        for(let element of buttonElements){

            element.disabled = true
        }
        
    }

    static activarForm(formID){
        var formElement = document.getElementById(formID)
        var inputElements = formElement.getElementsByTagName('input');
        var selectElements = formElement.getElementsByTagName('select');
        var buttonElements = formElement.getElementsByTagName('button');
        
        for(let element of inputElements){
            element.disabled = false
        }

        for(let element of selectElements){

            element.disabled = false
        }
        for(let element of buttonElements){

            element.disabled = false
        }
        
    }



    
}