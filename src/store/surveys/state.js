export default {
  survey: null,
  questions: null,
  answers: null,
  averages: null,
  counters: null
}
